const db = require('../db');

class TaskItemController {
  async getAll(req, res) {
    const task_items = await db.query('SELECT * FROM task_items');
    res.json(task_items.rows);
  }
}
