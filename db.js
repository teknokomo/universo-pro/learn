const Pool = require('pg').Pool
const pool = new Pool({
  host: 'localhost',
  database: 'wms',
  user: 'admin',
  password: 'secret',
  port: 5432,
})

module.exports = pool;
