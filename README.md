# Цели проекта
Создать платформу для обучения веб-программированию с нуля.

## Структура проекта

### Спецификация API
[Swagger UI на локальном сервере разработки](http://127.0.0.1:3000/swagger-ui/#/default)

### dist
Здесь будет создаваться артефакт с помощью инструмента автоматизированной сборки Jenkins.

### public
Здесь находятся статичные файлы сайта

# Разработка

Установить nodemon для автоматической перезагрузки локального сервера при изменении файлов JS.
```
npm install -D nodemon
```

Дописать в скрипты запуска в файле package.json строку
```
"dev": "nodemon server.js"
```

Запустить локальный сервер под nodemon
```
npm run dev
```

# Docker
## Сборка образа Docker

Собрать образ у себя на компе
```
docker build -t registry.gitlab.com/teknokomo/universo-pro/learn .
```

Войти в хранилище Docker проекта в Gitlab
```
docker login registry.gitlab.com
```

Поместить собранный образ в хранилище Docker в Gitlab
```
docker push registry.gitlab.com/teknokomo/universo-pro/learn
```

## Создание сети Docker
```
sudo docker network create lup-net1
```

## Запуск контейнера Docker
```
sudo docker run \
     --name lup-app01 \
     --env DEBUG=express \
     --env APP_PORT=3000 \
     --env CLIENT_ID=$CLIENT_ID \
     --env CLIENT_SECRET=$CLIENT_SECRET \
     --network lup-net1 \
     --publish 127.0.0.1:3000:3000 \
     --detach \
     registry.gitlab.com/teknokomo/universo-pro/learn
```