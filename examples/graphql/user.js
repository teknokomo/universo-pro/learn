axios.post(
  'https://t34.universo.pro/api/v1.1/registrado/',
  {
    // operationName: "mi",
    query: `query {
      mi {
        id: objId
        first_name: unuaNomo {
          value: enhavo
        }
        last_name: familinomo {
          value: enhavo
        }
      }
    }`
  },
  {
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${access_token}`
    }
  }
).then(response => {
  console.log(JSON.stringify(response.data, null, 2));

  res.send(`<a href=\'/logout'>logout</a><br>User data SUCCESS`);
}).catch(error => {
  if (error.response) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
  else if (error.request) {
    console.log(error.request);
  }
  else {
    console.log('Error', error.message);
  }
  console.log(error.config);

  res.send(`<a href=\'/logout'>logout</a><br>User data ERROR`);
});
