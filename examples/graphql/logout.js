
  
  axios.post(
    // Завершить сессию на сервере авторизации
    'https://t34.universo.pro/api/v1.1/registrado/',
    {
      query: `
        mutation {
          elsaluti {
            status
            message
          }
        }
      `
    },
    {
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${access_token}`
      }
    }
  ).then(response => {
    console.log('THEN');
    console.log(JSON.stringify(response.data, null, 2));
  }).catch(error => {
    console.log('CATCH');
    if (error.response) {
      console.log(JSON.stringify(response.data, null, 2));
      console.log(error.response.status);
      console.log(error.response.headers);
    }
    else if (error.request) {
      console.log(error.request);
    }
    else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  });

  // Успешный ответ
  // {
  //   "data": {
  //     "elsaluti": {
  //       "status": true,
  //       "message": "Аутентификация анулирована"
  //     }
  //   }
  // }

  // Выход осуществлён ранее
  // {
  //   "data": {
  //     "elsaluti": {
  //       "status": false,
  //       "message": "Требуется авторизация"
  //     }
  //   }
  // }