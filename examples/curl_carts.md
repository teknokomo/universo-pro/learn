# В метод на стороне сервера приходят
```
console.log('headers', req.headers);
console.log('params', req.params);
console.log('query', req.query);
console.log('body', req.body);
```

# Установка curl
```
sudo apt install curl
```

# Образец запроса POST с передачей всего, что только можно передать
```
curl \
  --request POST \
  --location 'http://127.0.0.1:3000/api/carts/123?new=true&name=qwe' \
  --header 'Content-Type: application/json' \
  --data '{"key1": "value1", "key2": true}'
```

# Вывод списка корзин
```
curl \
  --request GET \
  --location 'http://127.0.0.1:3000/api/carts' | jq
```

# Добавление корзины
```
curl \
  --request POST \
  --location 'http://127.0.0.1:3000/api/carts' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Корзина 1"}'

curl \
  --request POST \
  --location 'http://127.0.0.1:3000/api/carts' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Корзина 2"}'
```

# Получение конкретной корзины
```
curl \
  --request GET \
  --location 'http://127.0.0.1:3000/api/carts/2' | jq
```

# Замена корзины
```
curl \
  --request PUT \
  --location 'http://127.0.0.1:3000/api/carts/2' \
  --header 'Content-Type: application/json' \
  --data '{"id": 5, "name": "Новая корзина"}'
```

# Изменение параметров корзины
```
curl \
  --request PATCH \
  --location 'http://127.0.0.1:3000/api/carts/3' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Изменённая корзина"}'
```

# Удаление корзины
```
curl \
  --request DELETE \
  --location 'http://127.0.0.1:3000/api/cart/3'
```

# Добавление позиции в корзину
```
curl \
  --request POST \
  --location 'http://127.0.0.1:3000/api/carts/2/items' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Товар 1", "quantity": 1}'

curl \
  --request POST \
  --location 'http://127.0.0.1:3000/api/carts/2/items' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Товар 2", "quantity": 1}'
```

# Изменение данных элемента корзины
```
curl \
  --request PATCH \
  --location 'http://127.0.0.1:3000/api/carts/2/items/1' \
  --header 'Content-Type: application/json' \
  --data '{"name": "Новый товар", "quantity": 2, "picked": true}'
```

# Удалить элемент из корзины
```
curl \
  --request DELETE \
  --location 'http://127.0.0.1:3000/api/carts/2/items/1' \
  --header 'Content-Type: application/json'
```
