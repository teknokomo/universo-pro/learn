// import crypto from 'crypto';
const crypto = require('crypto');
const randomstring = require("randomstring");

var code_verifier = randomstring.generate(128).toString('base64url');
// var code_verifier = '2be912b663d2ab6fe34522f185b21e2a7c409f982cc4b53ccc7f1de6b62a3be9f35ff7821f0827fba287a322edd47e9b6c97cf4c31a235748067691763d7434f';
console.log('code_verifier');
console.log(code_verifier);

var code_challenge = crypto.createHash('sha256').update(code_verifier).digest('base64url');
console.log('code_challenge');
console.log(code_challenge);


// The client then creates a code challenge derived from the code
//    verifier by using one of the following transformations on the code
//    verifier:

//    plain
//       code_challenge = code_verifier

//    S256
//       code_challenge = BASE64URL-ENCODE(SHA256(ASCII(code_verifier)))