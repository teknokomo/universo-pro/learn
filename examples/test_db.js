const Pool = require('pg').Pool
const pool = new Pool({
  host: 'localhost',
  database: 'wms',
  user: 'admin',
  password: 'secret',
  port: 5432,
})

pool.query('INSERT INTO task_items (name, quantity) VALUES ($1, $2) RETURNING *', ['Колбаса', 1], (error, results) => {
  if (error) {
    throw error
  }

  console.log(JSON.stringify(results, ' ', 2));
  // response.status(201).send(`User added with ID: ${results.rows[0].id}`)
})
