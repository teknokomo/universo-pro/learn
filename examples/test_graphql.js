// Возможно потом понадобятся
// express-graphql - сервер GraphQL
// graphql

const axios = require('axios');

const req = {
  session: {
    access_token: 'LPSWhTg16Abkns7CBxlCOpfYZMJs6o'
  }
}

axios.post(
  'https://t34.universo.pro/api/v1.1/registrado/',
  {
    query: `query {
      mi {
        id: objId
        first_name: unuaNomo {
          value: enhavo
        }
        last_name: familinomo {
          value: enhavo
        }
      }
    }`
  },
  {
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${req.session.access_token}`
    }
  }
).then(response => {
  console.log(JSON.stringify(response.data, null, 2));
}).catch(error => {
  if (error.response) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
  else if (error.request) {
    console.log(error.request);
  }
  else {
    console.log('Error', error.message);
  }
  console.log(error.config);
});

// А вот так можно использовать альясы в #GraphQL

// Обычный запрос:

//   query: `query {
//     mi {
//       id
//       unuaNomo {
//         enhavo
//       }
//       familinomo {
//         enhavo
//       }
//     }
//   }`
// }
// Ответ:
// {
//   "data": {
//     "mi": {
//       "id": "TWlVemFudG9Ob2RlOjY3MTQ=",
//       "unuaNomo": {
//         "enhavo": "Александр"
//       },
//       "familinomo": {
//         "enhavo": "Туманов"
//       }
//     }
//   }
// }

// А вот так с альясами:
// {
//   user: mi {
//     id: objId
//     firstName: unuaNomo {
//       value: enhavo
//     }
//     lastName:familinomo {
//       value: enhavo
//     }
//   }
// }
// Ответ
// {
//   "data": {
//     "user": {
//       "id": 6714,
//       "firstName": {
//         "value": "Александр"
//       },
//       "lastName": {
//         "value": "Туманов"
//       }
//     }
//   }
// }


    // Примеры запросов /var/www/pro/universo/bot
    // Запрос к t34.universo.pro/api/v1.1/jwt/
//     const mi = gql`
//   query Mi {
//     mi {
//       id
//       objId
//       uuid
//       unuaNomo {
//         enhavo
//       }
//       duaNomo {
//         enhavo
//       }
//       familinomo {
//         enhavo
//       }
//       sekso
//       konfirmita
//       isActive
//       chefaLingvo {
//         id
//         nomo
//       }
//       chefaTelefonanumero
//       chefaRetposhto
//       naskighdato
//       loghlando {
//         nomo {
//           lingvo
//           enhavo
//           chefaVarianto
//         }
//         id
//       }
//       loghregiono {
//         id
//         nomo {
//           enhavo
//         }
//       }
//       agordoj
//       avataro {
//         id
//         bildoE {
//           url
//         }
//         bildoF {
//           url
//         }
//       }
//       statuso {
//         enhavo
//       }
//       statistiko {
//         miaGekamarado
//         miaGekamaradoPeto
//         kandidatoGekamarado
//         tutaGekamaradoj
//         rating
//         aktivaDato
//       }
//       kontaktaInformo
//       kandidatojTuta
//       gekamaradojTuta
//       chefaOrganizo {
//         id
//         nomo {
//           enhavo
//         }
//       }
//       sovetoj {
//         edges {
//           node {
//             id
//             nomo {
//               enhavo
//             }
//           }
//         }
//       }
//       sindikatoj {
//         edges {
//           node {
//             id
//             nomo {
//               enhavo
//             }
//           }
//         }
//       }
//       administritajKomunumoj {
//         edges {
//           node {
//             id
//             nomo {
//               enhavo
//             }
//           }
//         }
//       }
//       universoUzanto {
//         id
//         uuid
//         retnomo
//       }
//       isAdmin
//     }
//   }
// `


// const {data} = await axios.post('https://httpbin.org/post', {
//   firstName: 'Fred',
//   lastName: 'Flintstone',
//   orders: [1, 2, 3]
// }, {
//   headers: {
//     'Content-Type': 'application/x-www-form-urlencoded'
//   }
// })
