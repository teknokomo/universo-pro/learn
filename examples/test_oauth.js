// Запрашиваем токен пользователя у сервера:
// curl \
//   -X POST \
//   -H "Cache-Control: no-cache" \
//   -H "Content-Type: application/x-www-form-urlencoded" \
//   "https://t34.universo.pro/o/token/" \
//   -d "client_id=${ID}" \
//   -d "client_secret=${SECRET}" \
//   -d "code=${CODE}" \
//   -d "code_verifier=${CODE_VERIFIER}" \
//   -d "redirect_uri=http://t34.universo.pro/noexist/callback" \
//   -d "grant_type=authorization_code"

// https://t34.universo.pro/o/authorize/?
// response_type=code&
// code_challenge=RPoJ4cZKG8-gCSonREUH8Lq3mK7HlBWPiL2_Zg1GMD0&
// code_challenge_method=S256&
// client_id=LjfmJBLB6g1E0kknNJaeAEMGLXO73gVqmNDYadGa&
// redirect_uri=http://t34.universo.pro/noexist/callback

// Рекомендация для параметра scope
// https://curity.io/resources/learn/scope-best-practices/
// У нас пока не используется, так как пользователю даются все права на все API.


// https://datatracker.ietf.org/doc/html/rfc6749
// The OAuth 2.0 Authorization Framework
// https://datatracker.ietf.org/doc/html/rfc7636
// Proof Key for Code Exchange by OAuth Public Clients

// Описание работы OAuth на стороне сервера.
// Django OAuth Toolkit - https://django-oauth-toolkit.readthedocs.io/en/latest/index.html
