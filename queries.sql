-- Создать БД
CREATE TABLE task_items(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255),
  quantity INT,
  completed BOOLEAN DEFAULT false
);

-- Создать запись
INSERT INTO task_items (name, quantity)
VALUES ('Хлеб', 2);

-- Обновить запись
UPDATE task_items
SET quantity = 1
WHERE id = 2;

-- Удалить запись
DELETE FROM task_items
WHERE id = 2;
