'use strict';

// https://code.mu/ru/javascript/nodejs/book/express/

const express = require('express');                       // фреймворк ExpressJS
const cors = require('cors');                             // CORS
const axios = require('axios');                           // обмен с внешними источниками данных
const session = require('express-session');               // поддержка сессий
const expressHandlebars = require('express-handlebars');  // шаблонизатор страниц
const crypto = require('crypto');                         // шифрование одноразовых ключей PKCE
const randomstring = require("randomstring");             // создание случайной строки для PKCE

// Подключение маршрутов
const cartsApiRoutes = require('./routes/carts-api-routes');

const APP_INTERFACE = process.env.APP_INTERFACE || '0.0.0.0';
const APP_PORT = process.env.APP_PORT || 3000;

if (! process.env.CLIENT_ID) {
  console.log('Не найдена переменная среды CLIENT_ID. Сервер не сможет получать токены пользователей. Выход.');
  return;
}
const CLIENT_ID = process.env.CLIENT_ID;

if (! process.env.CLIENT_SECRET) {
  console.log('Не найдена переменная среды CLIENT_SECRET. Сервер не сможет получать токены пользователей. Выход.');
  return;
}
const CLIENT_SECRET = process.env.CLIENT_SECRET;

const REDIRECT_URI = process.env.PRODUCTION ? 'https://learn.universo.pro/auth/callback' : 'http://127.0.0.1:3000/auth/callback';

const USER_DATA_ENDPOINT = 'https://t34.universo.pro/api/v1.1/registrado/';
const APP_DATA_ENDPOINT = 'http://t34.universo.pro/api/v1.1/jwt/';

const oneDay = 1000 * 60 * 60 * 24;

const app = express();

// Подключение шаблонизатора
const handlebars = expressHandlebars.create({
	defaultLayout: 'main', 
	extname: 'hbs'
});

app.engine('hbs', handlebars.engine);
app.set('view engine', 'hbs');

// Настройка сессии

// Warning The default server-side session storage, MemoryStore, is purposely not designed
// for a production environment. It will leak memory under most conditions, does not scale
// past a single process, and is meant for debugging and developing.

var session_config = {
  secret: process.env.LEARN_UNIVERSO_PRO_SESSION_SECRET || 'iVPu588YBhlJFuIB',
  saveUninitialized: true,
  cookie: { maxAge: oneDay },
  resave: false
}

if (app.get('env') === 'production') {
  app.set('trust proxy', 1);                // доверять первому прокси (Nginx)
  session_config.cookie.secure = true;      // serve secure cookies
}

app.use(session(session_config));

app.use(express.json());                          // декодирует данные в формате JSON
app.use(express.urlencoded({ extended: true }));  // декодирует данные в формате URL-encoded

app.use(cors());

// Весь статический контент находится в папке public
// В этой папке нельзя делать подпапки. Всё нужно класть в корень. Иначе будут проблемы с определением типа файла.
// app.use('/css', express.static('public/css'));
app.use(express.static('public'));

function authorize(req, res, next) {
  if (
    req.session.user
    && req.session.user.access_token
  ) {
    return next();
  }

  res.redirect('/login');
}

// const taskItemsRouter = require('./routes/task_items_routes');

// app.use('/api', taskItemsRouter);

app.get('/login', (req, res) => {
  if (! req.session.user) {
    req.session.user = {
      'pkce': {}
    }

    req.session.user.pkce.code_verifier = randomstring.generate(128).toString('base64url');
    req.session.user.pkce.code_challenge = crypto.createHash('sha256').update(req.session.user.pkce.code_verifier).digest('base64url');
  }

  if (req.session.user.access_token) {
    res.redirect('/user');

    return;
  }

  let universo_login = `https://t34.universo.pro/o/authorize/?response_type=code&code_challenge=${req.session.user.pkce.code_challenge}&code_challenge_method=S256&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}`;
  res.render(
    'auth/login',
    {
      universo_login: universo_login
    }
  );
});

// Express преобразует путь в регулярное выражение. Можно использовать символы "+", "?", "*" "(" и ")". 
app.get('/auth/callback', (req, res) => {
  const code = req.query.code;

  axios.post(
    'https://t34.universo.pro/o/token/',
    {
      'grant_type': 'authorization_code',
      'client_id': CLIENT_ID,
      'client_secret': CLIENT_SECRET,
      'code': code,
      'code_verifier': req.session.user.pkce.code_verifier || 'error_no_code',
      'redirect_uri': REDIRECT_URI
    },
    {
      headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
  ).then(response => {
    req.session.user.access_token = response.data.access_token;

    res.redirect('/user');
    return;
  }).catch(error => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log('error.response.data');
      console.log(error.response.data);
      console.log('error.response.status');
      console.log(error.response.status);
      console.log('error.response.headers');
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log('error.request');
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
    console.log('error.config');
    console.log(error.config);
    res.send('CATCH');
  })
});

// Защищённые области должны вначале пройти проверку функцией authorize
// Можно делать цепочки из промежуточных функций.
app.get('/user', authorize, (req, res) => {
  // console.log(req.session);
  // console.log(req.session.user);
  // if (
  //   ! req.session.user
  //   || ! req.session.user.access_token
  // ) {
  //   res.redirect('/login');
    
  //   return;
  // }

  const access_token = req.session.user.access_token;

  axios.post(
    USER_DATA_ENDPOINT,
    {
      // operationName: "mi",
      query: `query {
        user: mi {
          id: objId
          first_name: unuaNomo {
            value: enhavo
          }
          last_name: familinomo {
            value: enhavo
          }
        }
      }`
    },
    {
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${access_token}`
      }
    }
  ).then(response => {
    console.log(JSON.stringify(response.data, null, 2));

    // res.send(`<a href=\'/logout'>logout</a><br>User data SUCCESS`);
    res.render(
      'auth/user',
      {
        user: {
          id: response.data.data.user.id,
          first_name: response.data.data.user.first_name.value,
          last_name: response.data.data.user.last_name.value
        }
      }
    );
  }).catch(error => {
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
    else if (error.request) {
      console.log(error.request);
    }
    else {
      console.log('Error', error.message);
    }
    console.log(error.config);

    res.send(`<a href=\'/logout'>logout</a><br>User data ERROR`);
  });
});

app.get('/logout', (req, res) => {
  if (
    ! req.session.user
    || ! req.session.user.access_token
  ) {
    res.redirect('/login');

    return;
  }

  const access_token = req.session.user.access_token;
  
  axios.post(
    // Завершить сессию на сервере авторизации
    USER_DATA_ENDPOINT,
    {
      query: `
        mutation {
          elsaluti {
            status
            message
          }
        }
      `
    },
    {
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${access_token}`
      }
    }
  ).then(response => {
    console.log('THEN');
    console.log(JSON.stringify(response.data, null, 2));

    req.session.destroy();
    res.redirect('/');

    return;
  }).catch(error => {
    console.log('CATCH');
    if (error.response) {
      console.log(JSON.stringify(response.data, null, 2));
      console.log(error.response.status);
      console.log(error.response.headers);
    }
    else if (error.request) {
      console.log(error.request);
    }
    else {
      console.log('Error', error.message);
    }
    console.log(error.config);

    res.redirect('/');

    return;
  });
});

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/schedule', (req, res) => {
  res.render('schedule');
});

app.get('/instructions/', (req, res) => {
  res.render('instructions/index');
});

app.get('/instructions/vscode', (req, res) => {
  res.render('instructions/vscode');
});

app.get('/instructions/certificate', (req, res) => {
  res.render('instructions/certificate');
});

app.get('/instructions/docker', (req, res) => {
  res.render('instructions/docker');
});

app.get('/instructions/heuristics', (req, res) => {
  res.render('instructions/heuristics');
});

app.get('/projects', (req, res) => {
  res.render('projects');
});

app.get('/task', (req, res) => {
  res.render('task/index');
});

app.get('/task/software', (req, res) => {
  res.render('task/software');
});

app.get('/task/learning', (req, res) => {
  res.render('task/learning');
});

app.get('/goal', (req, res) => {
  res.render('goal/index');
});

app.get('/goal/tick', (req, res) => {
  res.render('goal/tick');
});

app.get('/goal/cartridges', (req, res) => {
  res.render('goal/cartridges');
});

app.get('/about', (req, res) => {
  res.render('about');
});

app.get('/order', (req, res) => {
  res.render('list');
});

// Подключение маршрутов /api/carts
app.use(cartsApiRoutes);

//////////////////////////
// Пример того как передать обработку дальше по цепочке
// app.get('/list/:id', authorize, (req, res, next) => {
//   console.log('req.params.id', req.params.id);
//   switch (req.params.id) {
//     case '1':
//     case '2':
//       res.send(`req.params.id = ${req.params.id}`);
//     default:
//       next(); // 404
//   }
  
// });

app.listen(APP_PORT, APP_INTERFACE);
console.log(`running on http://${APP_INTERFACE}:${APP_PORT}`);
