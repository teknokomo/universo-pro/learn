const Router = require('express');
const router = new Router();
const taskItemController = require('../controllers/task_item_controller');

router.get('/task_items', taskItemController.getAll)

module.exports = router;
