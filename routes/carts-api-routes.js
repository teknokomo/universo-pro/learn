const {Router} = require('express');

const router = Router();

// Лучшие практики по проектированию API
// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/

// Cart
// id - идентификатор корзины
// name - название корзины

// Хранилище в памяти
let carts_next_id = 1;
let carts = [];

// CartItem
// id - идентификатор элемента в корзине
// cartId - идентификатор корзины
// name - название элемента
// quantity - количество элементов
// completed - индикатор того, что элемент собран

let carts_items_next_id = 1;
let carts_items = [];

let findItemsByCartId = cartId => {
  let items = [];
  for (let i = 0; i < carts_items.length; i++) {
    if (carts_items[i].cart_id == cartId) {
      items.push(carts_items[i]);
    }
  }

  return items;
};

let findCartById = id => {
  let cart = null;

  for (let i = 0; i < carts.length; i++) {
    if (id = carts[i].id) {
      cart = carts[i];
    }
  }

  return cart;
};

router.get('/api/carts', (req, res) => {
  console.log('GET /api/carts');
  for (let i; i < carts.length; i++) {
    let items = findItemsByCartId(carts[i].id);
    carts[i].items = items;
  }
  res.json(carts);
});

router.post('/api/carts', (req, res) => {
  // console.log('headers', req.headers); // Заголовки запроса
  // console.log('params', req.params);   // Это параметры пути (:id)
  // console.log('query', req.query);     // Это параметры запроса
  // console.log('body', req.body);       // Это данные запроса

  console.log('POST /api/carts');

  // Проверяем и очищаем полученное значение
  let name = req.body.name;

  let cart = {
    id: carts_next_id,
    name: name,
    items: []
  };

  carts.push(cart);
  carts_next_id++;

  res.json(cart);
});

router.get('/api/carts/:id', (req, res) => {
  let id = req.params.id;
  let cart = {};

  for (let i = 0; i < carts.length; i++) {
    if (id == carts[i].id) {
      cart = carts[i]
      cart.items = findItemsByCartId(carts[i].id);
      break;
    }
  }

  res.json(cart);
});

router.put('/api/carts/:id', (req, res) => {
  let id = req.params.id;
  let cart = {};

  for (let i = 0; i < carts.length; i++) {
    if (id == carts[i].id) {
      carts[i].id = req.body.id;
      carts[i].name = req.body.name;
      cart = carts[i]
      break;
    }
  }

  res.json(cart);
});

router.patch('/api/carts/:id', (req, res) => {
  let id = req.params.id;
  let cart = {};

  for (let i = 0; i < carts.length; i++) {
    if (id == carts[i].id) {
      if ("name" in req.body) {
        carts[i].name = req.body.name;
      }
      cart = carts[i]
      break;
    }
  }

  res.status(200).json(cart);
});

router.delete('/api/carts/:id', (req, res) => {
  let id = req.params.id;

  let cart = undefined;

  for (let i = 0; i < carts.length; i++) {
    if (id == carts[i].id) {
      cart = carts[i];
      carts.splice(i, 1);
      break;
    }
  }
  
  if (cart) {
    res.json({"success": true});
  }
  else {
    res.json({"success": false});
  }
});

router.post('/api/carts/:cart_id/items', (req, res) => {
  // console.log('headers', req.headers); // Заголовки запроса
  // console.log('params', req.params);   // Это параметры пути (:id)
  // console.log('query', req.query);     // Это параметры запроса
  // console.log('body', req.body);       // Это данные запроса

  let { cart_id } = req.params;

  if (! cart_id) {
    res.status(400).json({
      status: 400,
      error: "Не указан ID корзины"
    });
  }

  let cart = findCartById(cart_id);

  if (! cart) {
    res.status(400).json({
      status: 400,
      error: `Корзина с ID ${cart_id} не найдена`
    });
  }

  let cartItem = req.body;
  cartItem.id = carts_items_next_id;
  cartItem.cart_id = cart_id;
  cartItem.picked = false;

  carts_items.push(cartItem);
  carts_items_next_id++;

  res.status(200).json(cartItem);
});

router.patch('/api/carts/:cart_id/items/:item_id', (req, res) => {
  let { cart_id, item_id } = req.params;

  let cart = findCartById(cart_id);

  if (! cart) {
    res.status(400).json({
      status: 400,
      error: `Корзина с ID ${cart_id} не найдена`
    });

    return;
  }

  let cartItem = null;

  let cartItems = findItemsByCartId(cart_id);

  for (let i = 0; i < cartItems.length; i++) {
    if (cartItems[i].id == item_id) {
      if ("name" in req.body) {
        cartItems[i].name = req.body.name;
      }

      if ('quantity' in req.body) {
        cartItems[i].quantity = req.body.quantity;
      }

      if ('picked' in req.body) {
        cartItems[i].picked = req.body.picked;
      }

      cartItem = cartItems[i];
      break;
    }
  }
  
  if (cartItem) {
    res.status(200).json(cartItem);
  }
  else {
    res.status(400).json({
      status: 400,
      error: `Элемент с ID ${item_id} в корзине с ID ${cart_id} не найден`
    });
  }
});

router.delete('/api/carts/:cart_id/items/:item_id', (req, res) => {
  let { cart_id, item_id } = req.params;

  console.log('carts_items', carts_items);

  let cart = findCartById(cart_id);

  console.log(cart);

  if (! cart) {
    res.status(400).json({
      status: 400,
      error: `Корзина с ID ${cart_id} не найдена`
    });

    return;
  }

  let cartItem = null;

  console.log(carts_items);

  for (let i = 0; i < carts_items.length; i++) {
    if (carts_items[i].id == item_id) {
      cartItem = carts_items[i];
      carts_items.splice(i, 1);
      break;
    }
  }
  
  if (cartItem) {
    res.status(200).json(cartItem);
  }
  else {
    res.status(400).json({
      status: 400,
      error: `Элемент с ID ${item_id} в корзине с ID ${cart_id} не найден`
    });
  }
});

module.exports = router;
