FROM node:16-bullseye
WORKDIR /usr/src/app
# COPY package*.json ./
COPY . .
RUN npm install
# Если вы создаете сборку для продакшн
# RUN npm ci --only=production
EXPOSE 3000
CMD [ "node", "server.js" ]
